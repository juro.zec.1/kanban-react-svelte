module.exports = {
  env: {
    browser: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "airbnb-base",
    "airbnb-typescript/base",
    "plugin:import/recommended",
    "plugin:import/typescript",
  ],
  plugins: ["@typescript-eslint", "import", "unused-imports", "import-newlines"],
  parser: "@typescript-eslint/parser",
  settings: {
    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx", ".svelte"],
    },
    "import/resolver": {
      typescript: {
        alwaysTryTypes: true,
        project: ["tsconfig.json", "apps/*/tsconfig.json"],
        extensions: [".js", ".jsx", ".ts", ".tsx", ".svelte"]
      },
    },
  },
  rules: {
    // base
    "sort-imports": ["error", { ignoreDeclarationSort: true }],
    "no-unreachable": "warn",
    "no-restricted-exports": "off",
    "no-return-assign": "off",
    "max-len": "off",

    // import
    "import/no-extraneous-dependencies": ["error", { "devDependencies": true }],
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        "js": "never",
        "jsx": "never",
        "ts": "never",
        "tsx": "never",
        "svelte": "never",
      }
    ],

    // unused-imports
    "unused-imports/no-unused-imports": "error",
    "unused-imports/no-unused-vars": "warn",

    // import-newlines
    "import-newlines/enforce": ["error", { items: 1 }],
  },
  ignorePatterns: [
    "**/*.js",
    "**/*.json",
    "node_modules",
    "public",
    "styles",
    "dist",
    "generated",
    ".turbo",
  ],
};
