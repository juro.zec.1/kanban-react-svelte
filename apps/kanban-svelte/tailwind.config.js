const config = {
  content: [
    "./src/**/*.{html,js,svelte,ts}",
  ],
  theme: {
    extend: {
      colors: {
        // add custom colors here
      },
    },
  },
  plugins: [],
  darkMode: "class",
};

module.exports = config;
