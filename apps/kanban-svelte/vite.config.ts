import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';
import tsconfigPaths from 'vite-tsconfig-paths';
import analyzer from 'rollup-plugin-analyzer';
import { writeFileSync } from 'fs';

export default defineConfig({
  plugins: [
    svelte(),
    tsconfigPaths(),
  ],
  build: {
    rollupOptions: {
      plugins: [analyzer({ writeTo: (result) => writeFileSync('rollupAnalysis.log', result) })],
    },
  },
});
