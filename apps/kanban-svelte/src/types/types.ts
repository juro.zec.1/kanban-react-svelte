import type { Column } from 'generated/graphql';

export enum Theme {
  Light = 'light',
  Dark = 'dark',
}

export type DragStore = {
  issueId: string;
  sourceColumn: Column;
  destinationColumn: Column;
  isDragging: boolean;
  mouseX: number;
  mouseY: number;
};

export type Resolved<T> = [undefined, T] | [Error, undefined];
