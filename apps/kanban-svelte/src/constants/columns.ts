import { Column } from 'generated/graphql';

export const COLUMNS = [
  {
    id: Column.Todo,
    name: 'To do',
  },
  {
    id: Column.InProgress,
    name: 'In progress',
  },
  {
    id: Column.Done,
    name: 'Done',
  },
];
