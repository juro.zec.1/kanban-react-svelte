import type { User } from '@auth0/auth0-spa-js';

const userDataToMutation = (user: User) => ({
  data: {
    id: user.sub,
    email: user.email,
    name: user.nickname,
  },
});

export default userDataToMutation;
