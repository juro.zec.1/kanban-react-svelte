import type { Resolved } from 'types/types';

const resolve = <T>(promise: Promise<T>): Promise<Resolved<T>> => promise
  .then((data): [undefined, T] => [undefined, data])
  .catch((error): [Error, undefined] => [error, undefined]);

export default resolve;
