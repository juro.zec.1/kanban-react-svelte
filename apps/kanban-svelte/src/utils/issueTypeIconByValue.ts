import TaskIcon from 'icons/TaskIcon';
import BugIcon from 'icons/BugIcon';
import { IssueType } from 'generated/graphql';

const issueTypeIconByValue = (priority: IssueType) => {
  switch (priority) {
    case IssueType.Task:
      return TaskIcon;
    case IssueType.Bug:
      return BugIcon;
    default:
      return null;
  }
};

export default issueTypeIconByValue;
