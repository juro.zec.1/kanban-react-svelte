const generateId = (pow: number = 15): string => Math.round(Math.random() * 10 ** pow).toString(36);

export default generateId;
