import {
  derived,
  get,
  writable,
} from 'svelte/store';
import createAuth0Client, {
  type Auth0Client,
  type PopupLoginOptions,
  type User,
} from '@auth0/auth0-spa-js';
import resolve from 'utils/resolve';
import type { Kanban } from 'generated/graphql';

export const auth0Client = writable<Auth0Client>();
export const isAuthenticated = writable<boolean>(false);
export const authInitialized = writable<boolean>(false);
export const user = writable<Record<string, any>>({});
export const userId = writable<string>();
export const kanbans = writable<Kanban[]>();
export const kanbanShortId = writable<string>();
export const firstIssueCreated = writable<number>();

const findKanban = (kanbanList: Kanban[]): Kanban => kanbanList?.find((kanban) => kanban.shortId === get(kanbanShortId));

export const kanbanId = derived(kanbans, ($kanbans) => findKanban($kanbans)?.id);
export const kanbanName = derived(kanbans, ($kanbans) => findKanban($kanbans)?.name);
export const collaborators = derived(kanbans, ($kanbans) => findKanban($kanbans)?.collaborators);

export const initAuth0Client = async () => {
  const client = await createAuth0Client({
    domain: import.meta.env.VITE_AUTH0_DOMAIN,
    client_id: import.meta.env.VITE_AUTH0_CLIENT_ID,
  });
  auth0Client.set(client);
  const [resolvedIsAuthenticated, resolvedUser] = await Promise.all([
    client.isAuthenticated(),
    client.getUser(),
  ]);
  isAuthenticated.set(resolvedIsAuthenticated);
  user.set(resolvedUser);
  userId.set(resolvedUser?.sub || null);
  authInitialized.set(true);
};

export const loginWithPopup = async (options?: PopupLoginOptions): Promise<User> => {
  const client = get(auth0Client);
  if (!client) throw new Error('Auth0 not initialized.');
  const [authError] = await resolve(client.loginWithPopup(options));
  if (authError) {
    console.error(authError);
    return null;
  }

  const userData = await client.getUser();

  user.set(userData);
  isAuthenticated.set(true);
  userId.set(userData.sub);

  return userData;
};

export const logout = () => {
  const client = get(auth0Client);
  client.logout({
    returnTo: window.location.origin,
  });
  user.set({});
  isAuthenticated.set(false);
  userId.set(null);
};
