import {
  type Writable,
  writable,
} from 'svelte/store';
import { Theme } from 'types/types';

export const theme: Writable<string> = writable(Theme.Light);

const setDarkTheme = () => {
  document.documentElement.classList.add(Theme.Dark);
  document.documentElement.style.colorScheme = Theme.Dark;
  localStorage.svelteTheme = Theme.Dark;
  theme.set(Theme.Dark);
};

const setLightTheme = () => {
  document.documentElement.classList.remove(Theme.Dark);
  document.documentElement.style.colorScheme = Theme.Light;
  localStorage.svelteTheme = Theme.Light;
  theme.set(Theme.Light);
};

export const initTheme = () => {
  if (
    localStorage.svelteTheme === Theme.Dark
    || (!localStorage.svelteTheme && window.matchMedia('(prefers-color-scheme: dark)').matches)
  ) {
    setDarkTheme();
  } else setLightTheme();
};

export const toggleTheme = () => {
  if (localStorage.svelteTheme === Theme.Light) setDarkTheme();
  else setLightTheme();
};
