import {
  type Writable,
  derived,
  writable,
} from 'svelte/store';
import type { DragStore } from 'types/types';
import type { Issue } from 'generated/graphql';
import { Column } from 'generated/graphql';

export const dragStore: Writable<DragStore> = writable({
  isDragging: false,
  issueId: null,
  sourceColumn: null,
  destinationColumn: null,
  mouseX: null,
  mouseY: null,
});
export const issues = writable<Issue[]>();
export const isEmptyKanban = derived(issues, ($issues) => !Number.isInteger($issues?.length) || $issues.length === 0);

export const columnIssues = derived(issues, ($issues) => ({
  [Column.Todo]: $issues.filter((issue) => issue.column === Column.Todo) || [],
  [Column.InProgress]: $issues.filter((issue) => issue.column === Column.InProgress) || [],
  [Column.Done]: $issues.filter((issue) => issue.column === Column.Done) || [],
}));

export const updateDragStore = (values: Partial<DragStore>) => {
  dragStore.update((prev) => ({
    ...prev,
    ...values,
  }));
};
