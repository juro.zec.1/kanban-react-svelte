const eslintBase = require("eslintrc/base.js");

module.exports = {
  ...eslintBase,
  plugins: [...eslintBase.plugins, "svelte3"],
  overrides: [
    {
      files: ["*.svelte"],
      processor: "svelte3/svelte3"
    }
  ],
  settings: {
    ...eslintBase.settings,
    "svelte3/typescript": true,
  },
  rules: {
    ...eslintBase.rules,
    "no-multiple-empty-lines": "off",
    "no-console": ["warn", { allow: ["warn", "error"] }],
    "import/no-named-as-default": "off",
    "import/no-named-as-default-member": "off",
    "import/namespace": "off",
    "import/default": "off",
    "import/first": "off",
    "import/no-mutable-exports": "off",
    "import/prefer-default-export": "off",
  },
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json", "./tsconfig.node.json"],
    extraFileExtensions: [".svelte"],
  }
}
