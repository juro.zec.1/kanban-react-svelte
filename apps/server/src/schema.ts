import { makeSchema } from 'nexus';
import { Context } from './context';
import { User, UserCreateInput } from './schema/user';
import { Kanban, KanbanCreateInput, KanbanUpdateInput } from './schema/kanban';
import { Issue, IssueCreateInput, IssueUpdateInput } from './schema/issue';
import { Query } from './schema/query';
import { Mutation } from './schema/mutation';
import { IssueType, Priority, Column } from './schema/enums';
import { DateTime } from './schema/dateTime';

export const schema = makeSchema({
  types: [
    Query,
    Mutation,
    User,
    Kanban,
    Issue,
    UserCreateInput,
    IssueCreateInput,
    IssueUpdateInput,
    KanbanCreateInput,
    KanbanUpdateInput,
    IssueType,
    Column,
    Priority,
    DateTime,
  ],
  outputs: {
    schema: `${__dirname}/../schema.graphql`,
    typegen: `${__dirname}/generated/nexus.ts`,
  },
  contextType: {
    module: require.resolve('./context'),
    export: 'Context',
  },
  sourceTypes: {
    modules: [
      {
        module: '@prisma/client',
        alias: 'prisma',
      },
    ],
  },
});
