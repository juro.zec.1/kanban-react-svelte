import { enumType } from 'nexus';

export const IssueType = enumType({
  name: 'IssueType',
  members: ['TASK', 'BUG'],
  description: 'Issue type serves for quick categorization of the issue.',
})

export const Column = enumType({
  name: 'Column',
  members: ['TODO', 'IN_PROGRESS', 'DONE'],
  description: 'Column indicates progress of the issue.',
})

export const Priority = enumType({
  name: 'Priority',
  members: ['HIGHEST', 'HIGH', 'MEDIUM', 'LOW', 'LOWEST'],
  description: 'Priority decides the order in which issue should be worked on.',
})
