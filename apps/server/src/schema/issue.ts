import { inputObjectType, objectType } from 'nexus';
import { Context } from '../context';


export const Issue = objectType({
  name: 'Issue',
  definition(t) {
    t.nonNull.string('id');
    t.nonNull.int('shortId');
    t.nonNull.field('issueType', { type: 'IssueType' });
    t.nonNull.field('priority', { type: 'Priority' });
    t.nonNull.field('column', { type: 'Column' });
    t.nonNull.string('summary');
    t.nonNull.string('description');
    t.string('assigneeId');
    t.nonNull.field('createdAt', { type: 'DateTime' });
    t.nonNull.field('updatedAt', { type: 'DateTime' });
    t.field('kanban', {
      type: 'Kanban',
      resolve: (parent, _, context: Context) => context.prisma.issue
        .findUnique({
          where: { id: parent.id || undefined },
        })
        .kanban(),
    });
  },
});

export const IssueCreateInput = inputObjectType({
  name: 'IssueCreateInput',
  definition(t) {
    t.nonNull.field('issueType', { type: 'IssueType' });
    t.nonNull.field('priority', { type: 'Priority' });
    t.nonNull.field('column', { type: 'Column' });
    t.nonNull.string('summary');
    t.nonNull.string('description');
    t.string('assigneeId');
  },
});

export const IssueUpdateInput = inputObjectType({
  name: 'IssueUpdateInput',
  definition(t) {
    t.field('issueType', { type: 'IssueType' });
    t.field('priority', { type: 'Priority' });
    t.field('column', { type: 'Column' });
    t.string('summary');
    t.string('description');
    t.string('assigneeId');
  },
});
