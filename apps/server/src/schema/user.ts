import { inputObjectType, objectType } from 'nexus';
import { Context } from '../context';

export const User = objectType({
  name: 'User',
  definition(t) {
    t.nonNull.string('id');
    t.nonNull.string('email');
    t.nonNull.string('name');
    t.nonNull.list.nonNull.field('kanbans', {
      type: 'Kanban',
      resolve: (parent, _, context: Context) => context.prisma.user
        .findUnique({
          where: { id: parent.id || undefined },
        })
        .kanbans(),
    });
    t.nonNull.list.nonNull.field('collaborativeKanbans', {
      type: 'Kanban',
      resolve: (parent, _, context: Context) => context.prisma.user
        .findUnique({
          where: { id: parent.id || undefined },
        })
        .collaborativeKanbans(),
    });
  },
});

export const UserCreateInput = inputObjectType({
  name: 'UserCreateInput',
  definition(t) {
    t.nonNull.string('id');
    t.nonNull.string('email');
    t.nonNull.string('name');
    t.list.nonNull.field('kanbans', { type: 'KanbanCreateInput' });
    t.list.nonNull.field('collaborativeKanbans', { type: 'KanbanCreateInput' });
  },
});
