import { inputObjectType, objectType } from 'nexus';
import { Context } from '../context';

export const Kanban = objectType({
  name: 'Kanban',
  definition(t) {
    t.nonNull.string('id');
    t.nonNull.string('shortId');
    t.nonNull.int('incrementalIssueId');
    t.nonNull.string('name');
    t.nonNull.list.field('issues', {
      type: 'Issue',
      resolve: (parent, _, context: Context) => context.prisma.kanban
        .findUnique({
          where: { id: parent.id || undefined },
        })
        .issues(),
    });
    t.nonNull.list.field('collaborators', {
      type: 'User',
      resolve: (parent, _, context: Context) => context.prisma.kanban
        .findUnique({
          where: { id: parent.id || undefined },
        })
        .collaborators(),
    });
  },
});

export const KanbanCreateInput = inputObjectType({
  name: 'KanbanCreateInput',
  definition(t) {
    t.nonNull.string('name');
    t.nonNull.string('shortId');
    t.string('ownerId');
    t.list.field('issues', { type: 'IssueCreateInput' });
    t.list.field('collaborators', { type: 'UserCreateInput' });
  },
});

export const KanbanUpdateInput = inputObjectType({
  name: 'KanbanUpdateInput',
  definition(t) {
    t.int('incrementalIssueId');
    t.list.field('issues', { type: 'IssueCreateInput' });
    t.list.field('collaborators', { type: 'UserCreateInput' });
  },
});
