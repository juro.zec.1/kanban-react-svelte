import { arg, mutationType, nonNull, stringArg } from 'nexus';
import { Context } from '../context';
import { ApolloError } from 'apollo-server-express';

export const Mutation = mutationType({
  definition(t) {
    t.nonNull.field('createUser', {
      type: 'User',
      args: {
        data: nonNull(
          arg({
            type: 'UserCreateInput',
          }),
        ),
      },
      resolve: (_, args, context: Context) => {
        return context.prisma.user.create({
          data: {
            id: args.data.id,
            name: args.data.name,
            email: args.data.email,
          },
        });
      },
    });

    t.field('createKanban', {
      type: 'Kanban',
      args: {
        data: nonNull(
          arg({
            type: 'KanbanCreateInput',
          }),
        ),
        authorId: nonNull(stringArg()),
      },
      resolve: (_, args, context: Context) => {
        return context.prisma.kanban.create({
          data: {
            name: args.data.name,
            shortId: args.data.shortId,
            owner: {
              connect: { id: args.authorId }
            },
            collaborators: {
              connect: { id: args.authorId }
            },
            incrementalIssueId: 1,
          }
        });
      },
    });

    t.field('updateKanban', {
      type: 'Kanban',
      args: {
        data: nonNull(
          arg({
            type: 'KanbanUpdateInput',
          }),
        ),
        collaboratorEmail: nonNull(stringArg()),
        kanbanId: nonNull(stringArg()),
      },
      resolve: (_, args, context: Context) => {
        return context.prisma.kanban.update({
          where: { id: args.kanbanId },
          data: {
            collaborators: {
              connect: { email: args.collaboratorEmail }
            },
          }
        });
      },
    });

    t.field('createIssue', {
      type: 'Issue',
      args: {
        data: nonNull(
          arg({
            type: 'IssueCreateInput',
          }),
        ),
        authorId: nonNull(stringArg()),
        kanbanId: nonNull(stringArg()),
      },
      resolve: async (_, args, context: Context) => {
        const issueId = (await context.prisma.kanban.findUnique({
          where: { id: args.kanbanId },
        }))?.incrementalIssueId;

        if (!issueId) throw new ApolloError(`Failed to retrieve incrementalIssueId from kanban ${args.kanbanId}`);

        await context.prisma.kanban.update({
          where: { id: args.kanbanId },
          data: {
            incrementalIssueId: {
              increment: 1,
            },
          },
        })

        return context.prisma.issue.create({
          data: {
            shortId: issueId,
            issueType: args.data.issueType,
            priority: args.data.priority,
            column: args.data.column,
            summary: args.data.summary,
            description: args.data.description,
            assigneeId: args.data.assigneeId,
            kanban: {
              connect: { id: args.kanbanId }
            },
          }
        });
      },
    });

    t.field('updateIssue', {
      type: 'Issue',
      args: {
        data: nonNull(
          arg({
            type: 'IssueUpdateInput',
          }),
        ),
        authorId: nonNull(stringArg()),
        kanbanId: nonNull(stringArg()),
        issueId: nonNull(stringArg()),
      },
      resolve: async (_, args, context: Context) => {
        const issue = await context.prisma.issue.findUnique({
          where: { id: args.issueId },
        });

        if (!issue) throw new ApolloError(`Failed to retrieve issue to update, issueId: ${args.issueId}`);

        return context.prisma.issue.update({
          where: { id: args.issueId || undefined },
          data: {
            issueType: args.data.issueType || issue.issueType,
            priority: args.data.priority || issue.priority,
            column: args.data.column || issue.column,
            summary: args.data.summary || issue.summary,
            description: args.data.description || issue.description,
            assigneeId: args.data.assigneeId
          }
        });
      },
    });

    t.field('deleteIssue', {
      type: 'Issue',
      args: {
        issueId: nonNull(stringArg()),
      },
      resolve: (_, args, context: Context) => context.prisma.issue.delete({
        where: { id: args.issueId },
      }),
    });
  },
});
