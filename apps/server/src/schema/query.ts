import { nonNull, queryType, stringArg } from 'nexus';
import { Context } from '../context';

export const Query = queryType({
  definition(t) {
    t.nonNull.list.nonNull.field('allUsers', {
      type: 'User',
      resolve: (_parent, _args, context: Context) => context.prisma.user.findMany(),
    });

    t.nullable.field('user', {
      type: 'User',
      args: {
        userId: nonNull(stringArg()),
      },
      resolve: (_parent, args, context: Context) => context.prisma.user.findUnique({
        where: { id: args.userId },
      }),
    });

    t.nonNull.list.field('issues', {
      type: 'Issue',
      args: {
        kanbanId: nonNull(stringArg()),
      },
      resolve: (_parent, args, context: Context) => context.prisma.issue.findMany({
        where: { kanbanId: args.kanbanId },
      }),
    });
  },
});
