import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import express from 'express';
import http from 'http';
import cors from 'cors';
import { context } from './context';
import { schema } from './schema';

async function startApolloServer() {
  const app = express();
  app.use(cors());
  const httpServer = http.createServer(app);

  const server = new ApolloServer({
    schema,
    context,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
    introspection: true, // allow introspection for graphql-codegen
  });

  await server.start();

  server.applyMiddleware({
    app,
    path: '/graphql',
  });

  return new Promise<string>((resolve) => {
    httpServer.listen({ port: process.env.PORT || 4000 }, () => resolve(server.graphqlPath));
  });
}

startApolloServer().then((path) => console.log('server is listening at', process.env.NODE_ENV === 'production' ? path : 'http://localhost:4000/graphql'));
