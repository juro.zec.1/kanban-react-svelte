import { PrismaClient, Prisma } from '@prisma/client'

const prisma = new PrismaClient()

const USER_ID = 'auth0|6262db05a0945b0069d95e23';
const KANBAN_ID = '9e7d26e3-5796-4361-aa3b-19f9c9a6c336';
const MOCK_SHORT_ID = 1;

const issueData: Prisma.IssueCreateInput[] = [
  {
    issueType: 'BUG',
    priority: 'HIGH',
    column: 'TODO',
    summary: 'Fix a11y issue in login page',
    description: 'These issues need to be fixed ASAP.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'BUG',
    priority: 'LOW',
    column: 'TODO',
    summary: 'Investigate performance issues on left navigation panel',
    description: 'Logs keep reporting significant performance drop when clicking on arbitrary navitem.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'LOWEST',
    column: 'TODO',
    summary: 'Add internationalization to our page',
    description: 'Come out with generic i18n solution for our page.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'HIGHEST',
    column: 'TODO',
    summary: 'Implement option to export users from Auth',
    description: 'Management needs auth export for future company evaluation ASAP. In case of questions contact them.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'MEDIUM',
    column: 'TODO',
    summary: 'Investigate Testing library',
    description: 'There is this new testing framework. Check if we can switch.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'BUG',
    priority: 'MEDIUM',
    column: 'TODO',
    summary: 'Sharing doesn\'t work',
    description: 'Have a look at our sharing option, it seems to have some issues.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'MEDIUM',
    column: 'IN_PROGRESS',
    summary: 'Implement modals',
    description: 'We need modals for better UX, since our users complained about annoying redirects. Please implement reusable solution.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'HIGH',
    column: 'IN_PROGRESS',
    summary: 'Add Apple sign-in',
    description: 'Enable Apple as  sign-inprovider and add Apple sign-in button to our Login page.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'BUG',
    priority: 'MEDIUM',
    column: 'DONE',
    summary: 'Facebook users can\'t sign in',
    description: 'We need to fix sign in for our Facebook users.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'LOW',
    column: 'DONE',
    summary: 'Fix horizontal scroll issue in landing page',
    description: 'Horizontal scroll appears sometimes in landing page.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'TASK',
    priority: 'LOW',
    column: 'DONE',
    summary: 'Purge console logs',
    description: 'There is a lot of unnecessary console logs in production, please clean it.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
  {
    issueType: 'BUG',
    priority: 'MEDIUM',
    column: 'DONE',
    summary: 'Theme system preference doesn\'t work',
    description: 'Sensing theme preference from browser settings doesn\'t work, have a look at it please.',
    shortId: MOCK_SHORT_ID,
    assigneeId: USER_ID,
    kanban: {
      connect: {
        id: KANBAN_ID,
      },
    },
  },
];

async function main() {
  console.log(`Start seeding ...`)
  console.log(`Setting issues ...`)
  let incrementalIssueId = 1
  for (const data of issueData) {
    const issue = await prisma.issue.create({
      data: {
        ...data,
        shortId: incrementalIssueId++,
      },
    })
    console.log(`Created issue with id: ${issue.id}`)
  }
  console.log(`Setting issues finished.`)
  console.log(`Setting incrementalIssueId ...`)
  const kanban = await prisma.kanban.update({
    where: { id: KANBAN_ID },
    data: {
      incrementalIssueId,
    },
  })
  console.log(`Setting incrementalIssueId finished. New value: ${kanban.incrementalIssueId}`)
  console.log(`Seeding finished.`)
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
