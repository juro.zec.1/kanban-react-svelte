import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import analyzer from 'rollup-plugin-analyzer';
import { writeFileSync } from 'fs';

export default defineConfig({
  plugins: [
    react(),
    tsconfigPaths(),
  ],
  build: {
    rollupOptions: {
      plugins: [analyzer({ writeTo: (result) => writeFileSync('rollupAnalysis.log', result) })],
    },
  },
});
