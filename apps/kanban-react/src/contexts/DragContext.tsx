import {
  FC,
  createContext,
  useMemo,
  useState,
} from 'react';
import {
  Drag,
  DragContextType,
} from 'types/types';

const DragContext = createContext<DragContextType>({
  drag: {
    isDragging: false,
    issueId: undefined,
    sourceColumn: undefined,
    destinationColumn: undefined,
    mouseX: 0,
    mouseY: 0,
  },
  setDrag: () => undefined,
});

export const DragProvider: FC = ({ children }) => {
  const [drag, setDrag] = useState<Drag>({
    isDragging: false,
    mouseX: 0,
    mouseY: 0,
  });

  const memoizedValue = useMemo(() => ({
    drag,
    setDrag,
  }), [drag, setDrag]);

  return (
    <DragContext.Provider value={memoizedValue}>
      {children}
    </DragContext.Provider>
  );
};

export default DragContext;
