import {
  FC,
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  Theme,
  ThemeContextType,
} from 'types/types';

const ThemeContext = createContext<ThemeContextType>({
  theme: Theme.Light,
  isDark: false,
  toggleTheme: () => {},
});

export const ThemeProvider: FC = ({ children }) => {
  const [theme, setTheme] = useState(Theme.Light);
  const isDark = useMemo(() => theme === Theme.Dark, [theme]);

  const setDarkTheme = () => {
    document.documentElement.classList.add(Theme.Dark);
    document.documentElement.style.colorScheme = Theme.Dark;
    localStorage.reactTheme = Theme.Dark;
    setTheme(Theme.Dark);
  };

  const setLightTheme = () => {
    document.documentElement.classList.remove(Theme.Dark);
    document.documentElement.style.colorScheme = Theme.Light;
    localStorage.reactTheme = Theme.Light;
    setTheme(Theme.Light);
  };

  const toggleTheme = useCallback(() => {
    if (isDark) setLightTheme();
    else setDarkTheme();
  }, [isDark]);

  const initTheme = () => {
    if (
      localStorage.reactTheme === Theme.Dark
      || (!localStorage.reactTheme && window.matchMedia('(prefers-color-scheme: dark)').matches)
    ) setDarkTheme();
    else setLightTheme();
  };

  useEffect(initTheme, []);

  const memoizedValue = useMemo(() => ({
    theme,
    isDark,
    toggleTheme,
  }), [isDark, theme, toggleTheme]);

  return (
    <ThemeContext.Provider value={memoizedValue}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContext;
