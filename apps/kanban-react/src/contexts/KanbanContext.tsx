import {
  FC,
  createContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { KanbanContextType } from 'types/types';
import {
  Column,
  Issue,
  Kanban,
  User,
  useIssuesQuery,
  useUserQuery,
} from 'generated/graphql';
import { useAuth0 } from '@auth0/auth0-react';

const KanbanContext = createContext<KanbanContextType>({
  kanbans: undefined,
  issues: undefined,
  columnIssues: undefined,
  collaborators: undefined,
  kanbanShortId: undefined,
  setKanbanShortId: () => undefined,
  refetchUser: () => undefined,
  refetchIssues: () => undefined,
  kanbanId: undefined,
  kanbanName: undefined,
});

const findKanban = (kanbans?: Kanban[], kanbanShortId?: string): Kanban | undefined => kanbans?.find((kanban) => kanban.shortId === kanbanShortId);

export const KanbanProvider: FC = ({ children }) => {
  const [kanbans, setKanbans] = useState<Kanban[]>();
  const [issues, setIssues] = useState<Issue[]>();
  const [columnIssues, setColumnIssues] = useState<Record<Column, Issue[]>>();
  const [collaborators, setCollaborators] = useState<User[]>();
  const [kanbanShortId, setKanbanShortId] = useState<string>();
  const [kanbanId, setKanbanId] = useState<string>();
  const [kanbanName, setKanbanName] = useState<string>();
  const { user } = useAuth0();
  const [{ data: userData }, refetchUser] = useUserQuery({
    variables: { userId: user?.sub as string },
    pause: !user?.sub,
  });
  const [{ data: issueData }, refetchIssues] = useIssuesQuery({
    variables: { issuesKanbanId: kanbanId as string },
    pause: !kanbanId,
  });

  // useEffect(() => {
  //   if (!kanbanId || !issueCreated) return;
  //   if (!issues?.length || issueCreated) {
  //     refetchIssues({ requestPolicy: 'network-only' });
  //     setIssueCreated(null);
  //   }
  // }, [issueCreated, issues, kanbanId, refetchIssues]);

  // useEffect(() => {
  //   if (kanbanCreated) {
  //     refetchUser({ requestPolicy: 'network-only' });
  //   }
  // }, [kanbanCreated, refetchUser]);

  useEffect(() => {
    if (!userData?.user?.kanbans) return;
    setKanbans(userData.user.kanbans as Kanban[]);
  }, [userData]);

  useEffect(() => {
    if (!issueData?.issues) return;
    setIssues(issueData.issues as Issue[]);
  }, [issueData, userData]);

  useEffect(() => {
    if (!issues) return;
    setColumnIssues({
      [Column.Todo]: issues.filter((issue) => issue?.column === Column.Todo) || [],
      [Column.InProgress]: issues.filter((issue) => issue?.column === Column.InProgress) || [],
      [Column.Done]: issues.filter((issue) => issue?.column === Column.Done) || [],
    });
  }, [issues]);

  useEffect(() => {
    if (!kanbans) return;
    const kanban = findKanban(kanbans, kanbanShortId);
    setKanbanId(kanban?.id);
    setKanbanName(kanban?.name);
    setCollaborators(kanban?.collaborators as User[]);
  }, [kanbanShortId, kanbans]);

  const memoizedValue = useMemo(() => ({
    kanbans,
    issues,
    columnIssues,
    collaborators,
    kanbanShortId,
    setKanbanShortId,
    refetchUser,
    refetchIssues,
    kanbanId,
    kanbanName,
  }), [kanbans, issues, columnIssues, collaborators, kanbanShortId, refetchUser, refetchIssues, kanbanId, kanbanName]);

  return (
    <KanbanContext.Provider value={memoizedValue}>
      {children}
    </KanbanContext.Provider>
  );
};

export default KanbanContext;
