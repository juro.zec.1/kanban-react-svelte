import { useContext } from 'react';
import DragContext from 'contexts/DragContext';

const useDrag = () => useContext(DragContext);

export default useDrag;
