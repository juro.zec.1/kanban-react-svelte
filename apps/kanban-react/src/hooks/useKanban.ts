import { useContext } from 'react';
import KanbanContext from 'contexts/KanbanContext';

const useKanban = () => useContext(KanbanContext);

export default useKanban;
