import React from 'react';
import MainLayout from 'layouts/MainLayout';
import KanbanCard from 'components/KanbanCard';
import StartKanban from 'components/StartKanban';
import useKanban from 'hooks/useKanban';

const Landing: React.FC = () => {
  const { kanbans } = useKanban();

  return (
    <MainLayout>
      {Boolean(kanbans?.length) && (
        <div className="w-full h-[15%] absolute top-16 flex justify-center gap-8">
          <div className="w-2/3 grid grid-cols-3 gap-4">
            {kanbans?.map(({ name, shortId, collaborators }) => (
              <KanbanCard key={shortId} name={name} shortId={shortId} collaboratorCount={collaborators?.length} />
            ))}
          </div>
        </div>
      )}
      <div className="w-full flex justify-center absolute top-1/2 -translate-y-1/2">
        <StartKanban />
      </div>
    </MainLayout>
  );
};

export default Landing;
