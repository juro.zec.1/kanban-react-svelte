import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useKanban from 'hooks/useKanban';
import MainLayout from 'layouts/MainLayout';
import { DragProvider } from 'contexts/DragContext';
import Columns from 'components/Columns';
import CreateIssueButton from 'components/CreateIssueButton';
import AddCollaboratorButton from 'components/AddCollaboratorButton';

const Kanban: React.FC = () => {
  const { kanbanShortId } = useParams();
  const { setKanbanShortId } = useKanban();

  useEffect(() => {
    setKanbanShortId(kanbanShortId);

    return () => setKanbanShortId(undefined);
  }, [kanbanShortId, setKanbanShortId]);

  return (
    <MainLayout>
      <DragProvider>
        <div className="h-full w-10/12 mx-auto">
          <div className="flex gap-4 pt-6 items-center">
            <CreateIssueButton />
            <AddCollaboratorButton />
          </div>
          <Columns />
        </div>
      </DragProvider>
    </MainLayout>
  );
};

export default Kanban;
