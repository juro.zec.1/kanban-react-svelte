import React from 'react';
import Navbar from 'components/Navbar';

const MainLayout: React.FC = ({ children }) => (
  <>
    <Navbar />
    <div className="relative max-h-[calc(100vh-64px)] h-full overflow-y-auto overflow-x-hidden w-full">
      {children}
    </div>
  </>
);

export default MainLayout;
