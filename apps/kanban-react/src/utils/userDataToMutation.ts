import type { User } from '@auth0/auth0-react';
import { UserCreateInput } from 'generated/graphql';

const userDataToMutation = (user: User): UserCreateInput => {
  if (!user.sub || !user.email || !user.nickname) return {} as UserCreateInput;

  return {
    id: user.sub,
    email: user.email,
    name: user.nickname,
  };
};

export default userDataToMutation;
