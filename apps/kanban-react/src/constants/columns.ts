import { Column } from 'generated/graphql';

const COLUMNS = [
  {
    id: Column.Todo,
    name: 'To do',
  },
  {
    id: Column.InProgress,
    name: 'In progress',
  },
  {
    id: Column.Done,
    name: 'Done',
  },
];

export default COLUMNS;
