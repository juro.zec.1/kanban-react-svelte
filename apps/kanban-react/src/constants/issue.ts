import {
  Column,
  IssueType,
  Priority,
} from 'generated/graphql';

export const PRIORITY_OPTIONS = [
  {
    value: Priority.Highest,
    label: 'Highest',
  },
  {
    value: Priority.High,
    label: 'High',
  },
  {
    value: Priority.Medium,
    label: 'Medium',
  },
  {
    value: Priority.Low,
    label: 'Low',
  },
  {
    value: Priority.Lowest,
    label: 'Lowest',
  },
];

export const ISSUE_TYPE_OPTIONS = [
  {
    value: IssueType.Task,
    label: 'Task',
  },
  {
    value: IssueType.Bug,
    label: 'Bug',
  },
];

export const COLUMN_OPTIONS = [
  {
    value: Column.Todo,
    label: 'To do',
  },
  {
    value: Column.InProgress,
    label: 'In progress',
  },
  {
    value: Column.Done,
    label: 'Done',
  },
];
