import React from 'react';

const DialogBody: React.FC = ({ children }) => (
  <div className="p-6">
    {children}
  </div>
);

export default DialogBody;
