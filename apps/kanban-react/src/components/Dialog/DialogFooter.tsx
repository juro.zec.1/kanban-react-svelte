import React from 'react';

const DialogFooter: React.FC = ({ children }) => (
  <div
    className="flex items-center justify-end p-6 space-x-2 rounded-b border-t border-gray-200 dark:border-gray-600"
  >
    {children}
  </div>

);

export default DialogFooter;
