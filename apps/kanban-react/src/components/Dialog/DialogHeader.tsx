import React from 'react';
import CloseIcon from 'icons/CloseIcon';

type DialogHeaderProps = {
  onClose: () => void;
};

const DialogHeader: React.FC<DialogHeaderProps> = ({ children, onClose }) => (
  <div className="flex justify-between items-start p-5 rounded-t border-b dark:border-gray-600">
    <h3 className="text-xl font-semibold text-gray-900 lg:text-2xl dark:text-white">
      {children}
    </h3>
    <button
      type="button"
      onClick={onClose}
      className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto
      inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
    >
      <CloseIcon />
    </button>
  </div>
);

export default DialogHeader;
