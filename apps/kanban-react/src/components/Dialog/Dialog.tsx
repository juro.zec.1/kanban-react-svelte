import React from 'react';

const Dialog: React.FC = ({ children }) => (
  <div className="relative h-full md:h-auto">
    <div className="relative bg-white rounded-lg shadow-lg dark:bg-gray-800 w-full">
      {children}
    </div>
  </div>
);

export default Dialog;
