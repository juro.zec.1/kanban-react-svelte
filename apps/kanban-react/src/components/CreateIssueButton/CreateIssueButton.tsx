import React, { useState } from 'react';
import PlusIcon from 'icons/PlusIcon';
import IssueDialog from 'components/IssueDialog';

const CreateIssueButton: React.FC = () => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => setShowModal(true);

  const handleCloseModal = () => setShowModal(false);

  return (
    <>
      <button
        type="button"
        onClick={handleShowModal}
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg
    text-md px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
      >
        <PlusIcon className="mr-2 -ml-1" />
        Create issue
      </button>
      <IssueDialog
        show={showModal}
        onClose={handleCloseModal}
      />
    </>
  );
};

export default CreateIssueButton;
