import React, {
  useCallback,
  useEffect,
} from 'react';
import COLUMNS from 'constants/columns';
import useKanban from 'hooks/useKanban';
import useDrag from 'hooks/useDrag';
import { useUpdateIssueMutation } from 'generated/graphql';
import { useAuth0 } from '@auth0/auth0-react';
import Column from './Column';
import Card from './Card';

const Columns: React.FC = () => {
  const { columnIssues, kanbanId } = useKanban();
  const { drag, setDrag } = useDrag();
  const [, updateIssue] = useUpdateIssueMutation();
  const { user } = useAuth0();

  const handleMouseUp = useCallback(async () => {
    if (!drag.isDragging) return;
    if (!drag.destinationColumn || !drag.issueId || !kanbanId || !user?.sub) {
      setDrag((prev) => ({ ...prev, isDragging: false }));
      return;
    }
    await updateIssue({
      issueId: drag.issueId,
      authorId: user.sub,
      kanbanId,
      data: {
        column: drag.destinationColumn,
      },
    });
    setDrag((prev) => ({ ...prev, isDragging: false }));
  }, [drag, kanbanId, setDrag, updateIssue, user]);

  useEffect(() => {
    window.addEventListener('mouseup', handleMouseUp);

    return () => window.removeEventListener('mouseup', handleMouseUp);
  }, [handleMouseUp]);

  return (
    <div className="w-full grid grid-cols-3 gap-4 pt-8">
      {COLUMNS.map(({ id: column, name }) => (
        <Column key={column} column={column} name={name} issueCount={columnIssues?.[column]?.length}>
          {columnIssues?.[column].map((issue) => (
            <Card key={issue?.id} {...issue} />
          ))}
        </Column>
      ))}
    </div>
  );
};

export default Columns;
