/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
import React, {
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  Column,
  IssueType,
  Maybe,
  Priority,
} from 'generated/graphql';
import useDrag from 'hooks/useDrag';
import useKanban from 'hooks/useKanban';
import PriorityIconByValue from 'components/PriorityIconByValue';
import PersonIcon from 'icons/PersonIcon';
import IssueTypeIconByValue from 'components/IssueTypeIconByValue';
import IssueDialog from 'components/IssueDialog';

type CardProps = {
  id: string;
  shortId: number;
  issueType: IssueType;
  summary: string;
  description: string;
  priority: Priority;
  assigneeId?: Maybe<string>;
  column: Column;
  createdAt: string;
  updatedAt: string;
};

const TRIM_LENGTH = 80;

const Card: React.FC<CardProps> = ({
  id,
  shortId,
  issueType,
  summary,
  description,
  priority,
  assigneeId,
  column,
  createdAt,
  updatedAt,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [isDragEnabled, setIsDragEnabled] = useState(false);
  const [draggingThis, setDraggingThis] = useState(false);
  const [tx, setTx] = useState(0);
  const [ty, setTy] = useState(0);
  const [width, setWidth] = useState(0);
  const [startX, setStartX] = useState(0);
  const [startY, setStartY] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [satisfyClickRequest, setSatisfyClickRequest] = useState(false);
  const [assigneeName, setAssgineeName] = useState<string>();
  const { drag, setDrag } = useDrag();
  const { collaborators } = useKanban();

  useEffect(() => {
    setDraggingThis(isDragEnabled && drag.isDragging);
  }, [drag.isDragging, isDragEnabled]);

  useEffect(() => {
    if (!collaborators || !assigneeId) return;
    setAssgineeName(collaborators?.find((collabs) => assigneeId === collabs?.id)?.name);
  }, [assigneeId, collaborators, drag, isDragEnabled]);

  useEffect(() => {
    if (!ref.current) return;
    setWidth(ref.current.getBoundingClientRect().width);
  }, [assigneeId, collaborators, drag, isDragEnabled]);

  const handleShowModal = () => {
    if (!satisfyClickRequest) return;
    setShowModal(true);
    setSatisfyClickRequest(false);
  };

  const handleCloseModal = () => setShowModal(false);

  const handleDragStart = (event: React.MouseEvent<HTMLDivElement>) => {
    setSatisfyClickRequest(true);
    setStartX(event.clientX);
    setStartY(event.clientY);
    setDrag((prev) => ({
      ...prev,
      sourceColumn: column,
      issueId: id,
      mouseX: event.clientX,
      mouseY: event.clientY,
    }));
    setIsDragEnabled(true);
  };

  const handleDrag = (event: React.MouseEvent<HTMLDivElement>) => {
    if (!isDragEnabled) return;
    setTx(event.clientX - startX);
    setTy(event.clientY - startY);
    setDrag((prev) => ({
      ...prev,
      isDragging: true,
      mouseX: event.clientX,
      mouseY: event.clientY,
    }));
    setSatisfyClickRequest(false);
  };

  const handleDragEnd = () => {
    setIsDragEnabled(false);
    setTx(0);
    setTy(0);
  };

  return (
    <>
      <div
        ref={ref}
        onClick={handleShowModal}
        onMouseDown={handleDragStart}
        onMouseMove={handleDrag}
        onMouseUp={handleDragEnd}
        className={`select-none rounded-lg cursor-move p-2 mb-2 bg-slate-50 dark:bg-gray-700
        hover:bg-gray-200 hover:dark:bg-gray-900 ${draggingThis ? 'z-10 absolute shadow-2xl' : ''}`}
        style={{
          width: draggingThis ? `${width}px` : 'auto',
          transform: `translate(${tx}px, ${ty}px)`,
        }}
      >
        <div className="flex justify-between items-start">
          <h3 className="dark:text-white text-lg mb-2">{summary}</h3>
          <span className="p-0.5">
            <PriorityIconByValue priority={priority} />
          </span>
        </div>
        <p
          className="text-gray-400 dark:text-gray-400 text-sm"
        >
          {description.slice(0, TRIM_LENGTH)}
          {description.length > TRIM_LENGTH ? '...' : ''}
        </p>
        <pre>{'\n'}</pre>
        <div className="text-gray-500 dark:text-gray-400 flex justify-between">
          <span className="flex items-center gap-1.5">
            <IssueTypeIconByValue issueType={issueType} />
            {shortId}
          </span>
          <span className="flex items-center gap-1.5">
            <PersonIcon />
            {assigneeName || 'Unassigned'}
          </span>
        </div>
      </div>
      <IssueDialog
        show={showModal}
        onClose={handleCloseModal}
        id={id}
        shortId={shortId}
        issueType={issueType}
        summary={summary}
        column={column}
        description={description}
        priority={priority}
        assigneeId={assigneeId}
        createdAt={createdAt}
        updatedAt={updatedAt}
      />
    </>
  );
};

export default Card;
