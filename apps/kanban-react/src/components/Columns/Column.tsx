import React, {
  useEffect,
  useRef,
  useState,
} from 'react';
import { Column as EColumn } from 'generated/graphql';
import useDrag from 'hooks/useDrag';

type ColumnProps = {
  column: EColumn,
  issueCount?: number,
  name: string,
};

const Column: React.FC<ColumnProps> = ({
  column, issueCount = 0, name, children,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [enable, setEnable] = useState<boolean>(false);
  const [accept, setAccept] = useState<boolean>(false);
  const [columnHeight, setColumnHeight] = useState(0);
  const [coordsX, setCoordsX] = useState<number[]>([]);
  const [coordsY, setCoordsY] = useState<number[]>([]);
  const { drag, setDrag } = useDrag();

  useEffect(() => {
    if (!ref.current || !enable) return;
    const {
      x, y, width, height,
    } = ref.current.getBoundingClientRect();
    setCoordsX([x, x + width]);
    setCoordsY([y, y + height]);
  }, [enable]);

  useEffect(() => {
    if (!ref.current) return;
    const { height } = ref.current.getBoundingClientRect();
    setColumnHeight(height);
  }, [column, drag]);

  useEffect(() => {
    setEnable(drag?.isDragging && drag?.sourceColumn !== column);
  }, [column, drag]);

  useEffect(() => {
    const val = drag.mouseX >= coordsX[0] && drag.mouseX <= coordsX[1] && drag.mouseY >= coordsY[0] && drag.mouseY <= coordsY[1];
    setAccept(val);
  }, [coordsX, coordsY, drag]);

  useEffect(() => {
    if (!enable) return;
    setDrag((prev) => ({ ...prev, destinationColumn: accept ? column : undefined }));
  }, [accept, column, enable, setDrag]);

  return (
    <div className="w-full h-full pb-16">
      <div className="flex justify-between items-center">
        <h2 className="dark:text-white text-xl font-semibold mb-2">{name}</h2>
        <span className="text-gray-400 dark:text-gray-500 text-xl font-semibold mb-2 mr-1">{issueCount}</span>
      </div>
      <div
        ref={ref}
        style={{
          height: drag.isDragging ? `${columnHeight}px` : '',
        }}
        className={`relative p-2 w-full h-full rounded-lg shadow-slate-300 shadow-inner bg-white dark:bg-gray-800
          border dark:shadow-none dark:border-gray-700
        ${enable
          ? 'outline-2 outline-dashed shadow-none'
          : ''}
        ${enable && !accept
            ? 'outline-blue-500 bg-blue-50 dark:outline-blue-400 dark:bg-blue-900'
            : ''}
        ${enable && accept
              ? 'bg-green-100 outline-green-500 dark:bg-green-900 dark:outline-green-400'
              : ''}`}
      >
        {!enable && children}
      </div>
    </div>
  );
};

export default Column;
