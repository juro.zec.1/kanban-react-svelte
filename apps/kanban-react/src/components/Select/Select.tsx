import React from 'react';
import { Option } from 'types/types';

type SelectProps<T = string> = {
  id: string;
  value?: T;
  onChange: (value: T) => void;
  options: Option<T>[];
  label?: string;
  required?: boolean;
  hasIcon?: boolean;
  children?: React.ReactNode;
};

const Select = <T extends string>({
  id,
  value,
  onChange,
  label,
  options,
  required = false,
  hasIcon = false,
  children,
}: SelectProps<T>) => {
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault();
    onChange(event.target.value as T);
  };

  return (
    <>
      {Boolean(label) && (
        <label htmlFor={id} className="block pb-2 text-sm font-medium text-gray-900 dark:text-gray-400">
          {label}
          {required && <span className="text-red-600 dark:text-red-500">*</span>}
        </label>
      )}
      <div className="relative">
        <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none text-red-600">
          {children}
        </div>
        <select
          id={id}
          value={value || ''}
          onChange={handleChange}
          className={`bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-1 focus:ring-blue-500 focus:border-blue-500
            block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white
            hover:ring-1 hover:ring-blue-500 hover:border-blue-500 dark:hover:ring-blue-500 dark:hover:border-blue-500
            dark:focus:ring-blue-500 dark:focus:border-blue-500 ${hasIcon ? 'pl-10' : ''}`}
        >
          {!value && <option value="" disabled>{' '}</option>}
          {options.map((option) => (
            <option key={String(option.value)} value={option.value}>{option.label}</option>
          ))}
        </select>
      </div>
    </>
  );
};

export default Select;
