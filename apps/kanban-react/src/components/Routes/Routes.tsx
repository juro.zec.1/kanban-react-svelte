import React from 'react';
import {
  Navigate,
  Route,
  Routes as RouterRoutes,
} from 'react-router-dom';
import Kanban from 'routes/Kanban';
import Landing from 'routes/Landing';
import { KANBAN_PATH } from 'constants/paths';

const Routes: React.FC = () => (
  <RouterRoutes>
    <Route path="/" element={<Landing />} />
    <Route path={KANBAN_PATH} element={<Kanban />} />
    <Route path="*" element={<Navigate to="/" />} />
  </RouterRoutes>
);

export default Routes;
