import React from 'react';
import { Priority } from 'generated/graphql';
import PriorityHighestIcon from 'icons/PriorityHighestIcon';
import PriorityHighIcon from 'icons/PriorityHighIcon';
import PriorityMediumIcon from 'icons/PriorityMediumIcon';
import PriorityLowIcon from 'icons/PriorityLowIcon';
import PriorityLowestIcon from 'icons/PriorityLowestIcon';

type PriorityIconByValueProps = {
  priority: Priority;
};

const PriorityIconByValue: React.FC<PriorityIconByValueProps> = ({ priority }) => {
  switch (priority) {
    case Priority.Highest:
      return <PriorityHighestIcon />;
    case Priority.High:
      return <PriorityHighIcon />;
    case Priority.Medium:
      return <PriorityMediumIcon />;
    case Priority.Low:
      return <PriorityLowIcon />;
    case Priority.Lowest:
      return <PriorityLowestIcon />;
    default:
      return null;
  }
};

export default PriorityIconByValue;
