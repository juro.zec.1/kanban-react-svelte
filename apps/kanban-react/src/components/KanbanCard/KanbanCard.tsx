import { KANBAN_BASE_PATH } from 'constants/paths';
import React from 'react';
import { Link } from 'react-router-dom';
import ArrowRightIcon from 'icons/ArrowRightIcon';
import PersonIcon from 'icons/PersonIcon';

type KanbanCardProps = {
  name: string;
  shortId: string;
  collaboratorCount: number;
};

const KanbanCard: React.FC<KanbanCardProps> = ({ name, shortId, collaboratorCount }) => (
  <div className="p-3 flex flex-col max-w-sm bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h3 className="text-xl font-medium tracking-tight text-gray-900 dark:text-white">{name}</h3>
    <p className="font-normal text-gray-700 dark:text-gray-400">{shortId}</p>
    <div className="flex-grow" />
    <div className="flex justify-between items-end">
      <Link
        to={`${KANBAN_BASE_PATH}/${shortId}`}
        className="inline-flex items-center py-2 px-3 text-sm
      font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none
      focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
      >
        Go
        <ArrowRightIcon />
      </Link>
      <span className="flex items-center gap-1.5 text-gray-700 dark:text-gray-400">
        <PersonIcon />
        {collaboratorCount || 1}
      </span>
    </div>
  </div>
);

export default KanbanCard;
