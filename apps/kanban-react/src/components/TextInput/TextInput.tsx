import React, {useMemo} from 'react';

type TextInputProps = {
  id: string;
  value: string;
  onChange: (value: string) => void;
  label?: string;
  multiline?: boolean;
  placeholder: string;
  required?: boolean;
  className?: string;
  large?: boolean;
};

const TextInput: React.FC<TextInputProps> = ({
  id,
  value,
  onChange,
  label,
  multiline = false,
  placeholder,
  required = false,
  className,
  large = false,
}) => {
  const textInputClasses = useMemo(() => `bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-1 focus:ring-blue-500
  focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 hover:ring-1
  hover:ring-blue-500 hover:border-blue-500 dark:hover:ring-blue-500 dark:hover:border-blue-500 dark:text-white
  dark:focus:ring-blue-500 dark:focus:border-blue-500 ${className} ${large ? 'text-xl' : 'text-sm'}`, [className]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    event.preventDefault();
    onChange(event.target.value);
  };

  return (
    <>
      {Boolean(label) && (
        <label htmlFor={id} className="block pb-2 text-sm font-medium text-gray-900 dark:text-gray-400">
          {label}
          {required && <span className="text-red-600 dark:text-red-500">*</span>}
        </label>
      )}
      {!multiline ? (
        <input
          id={id}
          value={value}
          onChange={handleChange}
          placeholder={placeholder}
          type="text"
          required={required}
          className={textInputClasses}
        />
      ) : (
        <textarea
          id={id}
          value={value}
          onChange={handleChange}
          placeholder={placeholder}
          rows={4}
          className={textInputClasses}
        />
      )}
    </>
  );
};

export default TextInput;
