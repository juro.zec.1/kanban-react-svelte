import React, { useState } from 'react';
import PersonIcon from 'icons/PersonIcon';
import CollaboratorDialog from 'components/CollaboratorDialog';

const AddCollaboratorButton: React.FC = () => {
  const [show, setShow] = useState(false);

  const handleShowModal = () => setShow(true);

  const handleCloseModal = () => setShow(false);

  return (
    <>
      <button
        type="button"
        onClick={handleShowModal}
        className="py-2.5 px-5 font-medium text-gray-900 focus:outline-none bg-white
  text-md text-center inline-flex items-center
  rounded-lg border border-gray-200 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200
  dark:focus:ring-gray-700 hover:border-blue-500 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white
  dark:hover:bg-gray-700"
      >
        <PersonIcon className="mr-2 -ml-1" />
        Add collaborator
      </button>
      <CollaboratorDialog show={show} onClose={handleCloseModal} />
    </>

  );
};

export default AddCollaboratorButton;
