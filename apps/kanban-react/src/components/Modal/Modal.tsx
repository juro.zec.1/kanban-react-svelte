import React, {
  useEffect,
  useRef,
  useState,
} from 'react';

type ModalProps = {
  show: boolean;
};

const Modal: React.FC<ModalProps> = ({ show = false, children }) => {
  const ref = useRef<HTMLDivElement>(null);
  const [isOverflow, setIsOverflow] = useState<boolean>(false);

  useEffect(() => {
    if (!ref.current) return;
    const { height } = ref.current.getBoundingClientRect();
    setIsOverflow(height > window.innerHeight);
  }, [show]);

  return (
    <div
      tabIndex={-1}
      aria-hidden="true"
      className={`${!show ? 'hidden' : ''} fixed left-0 top-0 right-0 z-50 w-full md:inset-0 h-full
        backdrop-blur-sm overflow-y-auto overflow-x-hidden flex justify-center items-end`}
    >
      {show && (
        <div
          ref={ref}
          className={`absolute ${isOverflow ? 'top-0' : 'top-1/2 -translate-y-1/2'} w-full max-w-2xl`}
        >
          {children}
        </div>
      )}
    </div>
  );
};

export default Modal;
