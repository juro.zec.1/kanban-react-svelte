import React from 'react';
import { IssueType } from 'generated/graphql';
import TaskIcon from 'icons/TaskIcon';
import BugIcon from 'icons/BugIcon';

type IssueTypeIconByValueProps = {
  issueType: IssueType;
};

const IssueTypeIconByValue: React.FC<IssueTypeIconByValueProps> = ({ issueType }) => {
  switch (issueType) {
    case IssueType.Task:
      return <TaskIcon />;
    case IssueType.Bug:
      return <BugIcon />;
    default:
      return null;
  }
};

export default IssueTypeIconByValue;
