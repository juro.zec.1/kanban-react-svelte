import React, { useEffect } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import LogoutIcon from 'icons/LogoutIcon';
import { useCreateUserMutation } from 'generated/graphql';
import userDataToMutation from 'utils/userDataToMutation';

type SignInButtonProps = {
  className?: string,
};

const SignInButton: React.FC<SignInButtonProps> = ({ className }) => {
  const {
    isAuthenticated, user, logout, loginWithRedirect,
  } = useAuth0();
  const [, createUser] = useCreateUserMutation();

  const handleSignIn = () => loginWithRedirect();

  const handleLogout = () => logout();

  useEffect(() => {
    if (!user) return;
    createUser({ data: userDataToMutation(user) }).catch(console.error);
  }, [user, createUser]);

  return (
    isAuthenticated ? (
      <>
        <span className="dark:text-white">{user?.nickname}</span>
        <button
          type="button"
          onClick={handleLogout}
          className="text-gray-500 dark:text-gray-400 hover:text-black hover:dark:text-white hover:bg-gray-200 dark:hover:bg-gray-700
        dark:focus:ring-gray-700 rounded-lg text-sm p-2"
        >
          <LogoutIcon />
        </button>
      </>
    ) : (
      <button
        type="button"
        onClick={handleSignIn}
        className={`text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium
        rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 ${className}`}
      >
        Sign in
      </button>
    )
  );
};

export default SignInButton;
