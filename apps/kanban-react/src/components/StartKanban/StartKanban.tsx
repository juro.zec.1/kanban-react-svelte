import React, { useState } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import TextInput from 'components/TextInput';
import SignInButton from 'components/SignInButton';
import { useCreateKanbanMutation } from 'generated/graphql';
import generateId from 'utils/generateId';
import { useNavigate } from 'react-router-dom';
import { KANBAN_BASE_PATH } from 'constants/paths';
import useKanban from 'hooks/useKanban';

const StartKanban: React.FC = () => {
  const { isAuthenticated, user } = useAuth0();
  const { refetchUser } = useKanban();
  const [name, setName] = useState('');
  const [, createKanban] = useCreateKanbanMutation();
  const navigate = useNavigate();

  const handleCreateKanban = async (event: React.FormEvent) => {
    event.preventDefault();
    if (!name || !user?.sub) return;
    const kanbanShortId = generateId();
    await createKanban({
      data: {
        name,
        shortId: kanbanShortId,
      },
      authorId: user?.sub,
    });
    refetchUser({ requestPolicy: 'network-only' });
    navigate(`${KANBAN_BASE_PATH}/${kanbanShortId}`);
  };

  return (
    <div
      className="p-4 rounded-lg flex flex-col max-w-xl w-full items-center bg-white dark:bg-gray-800
      shadow-sm dark:shadow-none dark:border dark:border-gray-700"
    >
      <h2 className="dark:text-white text-3xl pb-3 font-semibold">Start new kanban</h2>
      <form className="w-full flex flex-col items-center" onSubmit={handleCreateKanban}>
        <TextInput
          value={name}
          onChange={setName}
          id="name"
          placeholder="Name your kanban"
          required
          large
          className="text-xl mt-8"
        />
        <button
          type="submit"
          disabled={!isAuthenticated}
          className={`text-white w-1/4 flex justify-center text-xl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium
            rounded-lg text-md px-5 py-2 mt-8 text-center inline-flex items-center  dark:focus:ring-blue-800
          ${isAuthenticated ? 'bg-gradient-to-r from-purple-600 to-blue-500' : 'bg-gray-400 dark:bg-gray-500 cursor-not-allowed'}`}
        >
          Start
        </button>
        {!isAuthenticated && <SignInButton className="text-md font-medium px-5 py-2 w-1/4 justify-center mt-3" />}
      </form>
    </div>
  );
};

export default StartKanban;
