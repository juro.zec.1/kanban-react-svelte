import React from 'react';
import SunIcon from 'icons/SunIcon';
import MoonIcon from 'icons/MoonIcon';
import useTheme from 'hooks/useTheme';

const ThemeButton: React.FC = () => {
  const { isDark, toggleTheme } = useTheme();

  return (
    <button
      onClick={toggleTheme}
      type="button"
      className="text-gray-500 dark:text-gray-400 hover:text-black hover:dark:text-white hover:bg-gray-200
      dark:hover:bg-gray-700 dark:focus:ring-gray-700 rounded-lg text-sm p-2"
    >
      {isDark ? <SunIcon /> : <MoonIcon />}
    </button>
  );
};

export default ThemeButton;
