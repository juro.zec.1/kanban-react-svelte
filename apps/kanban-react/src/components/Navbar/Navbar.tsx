import React from 'react';
import { Link } from 'react-router-dom';
import logo from 'assets/logo.png';
import SignInButton from 'components/SignInButton';
import useKanban from 'hooks/useKanban';
import ThemeButton from './ThemeButton';

const Navbar: React.FC = () => {
  const { kanbanName } = useKanban();

  return (
    <nav className="bg-white border-gray-200 p-2 sm:p-3 w-full dark:bg-gray-800">
      <div className="relative flex flex-wrap justify-between items-center">
        <Link to="/" className="flex items-center">
          <img src={logo} className="ml-1 mr-3 h-9" alt="Svelte kanban" />
          <h1 className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
            React Kanban
          </h1>
        </Link>
        {Boolean(kanbanName) && (
          <h3 className="text-xl absolute left-1/2 -translate-x-1/2 font-medium tracking-tight text-gray-900 dark:text-white">{kanbanName}</h3>
        )}
        <div className="flex items-center gap-2">
          <SignInButton />
          <ThemeButton />
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
