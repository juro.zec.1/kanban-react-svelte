import React, {
  useEffect,
  useMemo,
  useState,
} from 'react';
import Modal from 'components/Modal';
import {
  Dialog,
  DialogBody,
  DialogFooter,
  DialogHeader,
} from 'components/Dialog';
import {
  Column,
  IssueType,
  Maybe,
  Priority,
  useCreateIssueMutation,
  useDeleteIssueMutation,
  useUpdateIssueMutation,
} from 'generated/graphql';
import TextInput from 'components/TextInput';
import Select from 'components/Select';
import IssueTypeIconByValue from 'components/IssueTypeIconByValue';
import PriorityIconByValue from 'components/PriorityIconByValue';
import {
  COLUMN_OPTIONS,
  ISSUE_TYPE_OPTIONS,
  PRIORITY_OPTIONS,
} from 'constants/issue';
import useKanban from 'hooks/useKanban';
import { Option } from 'types/types';
import DeleteIcon from 'icons/DeleteIcon';
import { useAuth0 } from '@auth0/auth0-react';

type IssueDialogProps = {
  show: boolean,
  onClose: () => void,
  id?: string,
  shortId?: number,
  issueType?: IssueType,
  summary?: string,
  description?: string,
  priority?: Priority,
  column?: Column,
  assigneeId?: Maybe<string>,
  createdAt?: string,
  updatedAt?: string,
};

const IssueDialog: React.FC<IssueDialogProps> = ({
  show,
  onClose,
  id,
  shortId,
  issueType: initialIssueType = IssueType.Task,
  summary: initialSummary = '',
  description: initialDescription = '',
  priority: initialPriority = Priority.Medium,
  column: initialColumn = Column.Todo,
  assigneeId: initialAssigneeId,
  createdAt,
  updatedAt,
}) => {
  const [issueType, setIssueType] = useState(initialIssueType);
  const [summary, setSummary] = useState(initialSummary);
  const [description, setDescription] = useState(initialDescription);
  const [priority, setPriority] = useState(initialPriority);
  const [column, setColumn] = useState(initialColumn);
  const [assigneeId, setAssigneeId] = useState(initialAssigneeId);
  const [assigneeOptions, setAssigneeOptions] = useState<Option[]>([]);
  const isUpdate = useMemo(() => Boolean(id), [id]);
  const { collaborators, kanbanId, refetchIssues } = useKanban();
  const { user } = useAuth0();
  const [, createIssue] = useCreateIssueMutation();
  const [, updateIssue] = useUpdateIssueMutation();
  const [, deleteIssue] = useDeleteIssueMutation();

  useEffect(() => {
    if (!collaborators) return;
    setAssigneeOptions(collaborators?.map((collaborator) => ({ value: collaborator.id, label: collaborator.name })) || []);
  }, [collaborators]);

  const handleDelete = async () => {
    if (id) await deleteIssue({ issueId: id });
  };

  const handleSubmit = async (event: React.SyntheticEvent) => {
    event.preventDefault();
    if (!user?.sub || !kanbanId) return;
    const commonVariables = {
      authorId: user.sub,
      kanbanId,
      data: {
        assigneeId,
        column,
        description,
        issueType,
        priority,
        summary,
      },
    };
    if (isUpdate) {
      if (id) {
        await updateIssue({
          ...commonVariables,
          issueId: id,
        });
      }
    } else {
      await createIssue(commonVariables);
      refetchIssues({ requestPolicy: 'network-only' });
    }

    onClose();
  };

  return (
    <Modal show={show}>
      <Dialog>
        <DialogHeader onClose={onClose}>{isUpdate ? `Issue ${shortId}` : 'Create issue'}</DialogHeader>
        <form className="w-full" onSubmit={handleSubmit}>
          <DialogBody>
            <div className="mb-6">
              <Select
                value={issueType}
                onChange={setIssueType}
                id="issueType"
                label="Issue type"
                options={ISSUE_TYPE_OPTIONS}
                required
                hasIcon
              >
                <IssueTypeIconByValue issueType={issueType} />
              </Select>
            </div>
            <div className="mb-6">
              <TextInput
                value={summary}
                onChange={setSummary}
                id="summary"
                label="Summary"
                placeholder="Name your issue"
                required
              />
            </div>
            <div className="mb-6">
              <TextInput
                value={description}
                onChange={setDescription}
                id="description"
                label="Description"
                placeholder="Describe issue"
                required
                multiline
              />
            </div>
            <div className="mb-6">
              <Select
                value={priority}
                onChange={setPriority}
                id="priority"
                label="Priority"
                options={PRIORITY_OPTIONS}
                required
                hasIcon
              >
                <PriorityIconByValue priority={priority} />
              </Select>
            </div>
            <div className="mb-6">
              <Select
                value={column}
                onChange={setColumn}
                id="column"
                label="Column"
                options={COLUMN_OPTIONS}
                required
              />
            </div>
            <div className="mb-6">
              <Select
                value={assigneeId || ''}
                onChange={setAssigneeId}
                id="assignee"
                label="Assignee"
                options={assigneeOptions}
              />
            </div>
            {isUpdate && (
            <>
              <p className="block pb-2 text-sm font-medium text-gray-900 dark:text-gray-400">
                Created
                at:
                {createdAt ? new Date(createdAt).toLocaleString() : 'Unknown'}
              </p>
              <p className="block pb-2 text-sm font-medium text-gray-900 dark:text-gray-400">
                Updated
                at:
                {updatedAt ? new Date(updatedAt).toLocaleString() : 'Unknown'}
              </p>
            </>
            )}
          </DialogBody>
          <DialogFooter>
            <div className={`w-full flex ${isUpdate ? 'justify-between' : 'justify-end'}`}>
              {isUpdate && (
                <button
                  type="button"
                  onClick={handleDelete}
                  className="text-gray-500 hover:bg-gray-100 border hover:bg-red-700 hover:text-white focus:ring-4
                    focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm p-2.5 text-center inline-flex
                    items-center mr-2 dark:border-gray-500 dark:text-gray-500 dark:hover:text-white dark:focus:ring-red-800"
                >
                  <DeleteIcon />
                </button>
              )}
              <div>
                <button
                  type="button"
                  onClick={onClose}
                  className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg
                    border border-gray-200 mr-1 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700
                    dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium
                    rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  {isUpdate ? 'Save changes' : 'Create'}
                </button>
              </div>
            </div>
          </DialogFooter>
        </form>
      </Dialog>
    </Modal>

  );
};

export default IssueDialog;
