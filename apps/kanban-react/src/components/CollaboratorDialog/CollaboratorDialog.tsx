import React, { useState } from 'react';
import Modal from 'components/Modal';
import {
  Dialog,
  DialogBody,
  DialogFooter,
  DialogHeader,
} from 'components/Dialog';
import TextInput from 'components/TextInput';
import { useUpdateKanbanMutation } from 'generated/graphql';
import useKanban from 'hooks/useKanban';

type CollaboratorDialogProps = {
  show: boolean;
  onClose: () => void;
};

const CollaboratorDialog: React.FC<CollaboratorDialogProps> = ({ show, onClose }) => {
  const [email, setEmail] = useState('');
  const { kanbanId } = useKanban();
  const [, updateKanban] = useUpdateKanbanMutation();

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    if (!kanbanId) return;
    await updateKanban({ collaboratorEmail: email.trim(), kanbanId, data: {} });
    onClose();
  };

  return (
    <Modal show={show}>
      <Dialog>
        <DialogHeader onClose={onClose}>Add collaborator</DialogHeader>
        <form className="w-full" onSubmit={handleSubmit}>
          <DialogBody>
            <TextInput
              value={email}
              onChange={setEmail}
              id="email"
              label="Email"
              placeholder="name@example.com"
              required
            />
          </DialogBody>
          <DialogFooter>
            <button
              type="button"
              onClick={onClose}
              className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg
                border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700
                dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
            >
              Cancel
            </button>
            <button
              type="submit"
              className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium
                rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Add
            </button>
          </DialogFooter>
        </form>
      </Dialog>
    </Modal>
  );
};

export default CollaboratorDialog;
