import {
  Column,
  Issue,
  Kanban,
  User,
} from 'generated/graphql';
import {
  Dispatch,
  SetStateAction,
} from 'react';
import { OperationContext } from 'urql';

export enum Theme {
  Light = 'light',
  Dark = 'dark',
}

export type ThemeContextType = {
  theme: Theme;
  isDark: boolean;
  toggleTheme: () => void;
};

export type KanbanContextType = {
  kanbans?: Kanban[];
  issues?: Issue[];
  columnIssues?: Record<Column, Issue[]>;
  collaborators?: User[];
  kanbanShortId?: string;
  setKanbanShortId: (kanbanShortId?: string) => void;
  refetchUser: (opts?: (Partial<OperationContext> | undefined)) => void;
  refetchIssues: (opts?: (Partial<OperationContext> | undefined)) => void;
  kanbanId?: string;
  kanbanName?: string;
};

export type DragContextType = {
  drag: Drag;
  setDrag: Dispatch<SetStateAction<Drag>>;
};

export type Drag = {
  issueId?: string;
  sourceColumn?: Column;
  destinationColumn?: Column;
  isDragging: boolean;
  mouseX: number;
  mouseY: number;
};

export type Option<T = string> = {
  value: T,
  label: string,
};
