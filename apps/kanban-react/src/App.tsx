import {
  Provider as UrqlProvider,
  cacheExchange,
  createClient,
  debugExchange,
  fetchExchange,
} from 'urql';
import { Auth0Provider } from '@auth0/auth0-react';
import { ThemeProvider as ThemeContextProvider } from 'contexts/ThemeContext';
import { KanbanProvider as KanbanContextProvider } from 'contexts/KanbanContext';
import Routes from 'components/Routes';
import { BrowserRouter } from 'react-router-dom';

const client = createClient({
  url: import.meta.env.VITE_GRAPHQL_API_ENDPOINT,
  exchanges: [debugExchange, cacheExchange, fetchExchange],
});

const App = () => (
  <BrowserRouter>
    <ThemeContextProvider>
      <UrqlProvider value={client}>
        <Auth0Provider
          domain={import.meta.env.VITE_AUTH0_DOMAIN}
          clientId={import.meta.env.VITE_AUTH0_CLIENT_ID}
          redirectUri={window.location.origin}
        >
          <KanbanContextProvider>
            <main className="h-full flex flex-col items-center bg-gray-100 dark:bg-[#0b0f19]">
              <Routes />
            </main>
          </KanbanContextProvider>
        </Auth0Provider>
      </UrqlProvider>
    </ThemeContextProvider>
  </BrowserRouter>
);

export default App;
