import React from 'react';

const PriorityLowIcon: React.FC = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="20px" width="20px" fill="none" stroke="#057A55">
    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" d="M19 9l-7 7-7-7" />
  </svg>
);

export default PriorityLowIcon;
