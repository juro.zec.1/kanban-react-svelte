import React from 'react';

const PriorityHighIcon: React.FC = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="20px" width="20px" fill="none" stroke="#F05252">
    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" d="M5 15l7-7 7 7" />
  </svg>
);

export default PriorityHighIcon;
