import React from 'react';

const PriorityMediumIcon: React.FC = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="20px" width="20px" stroke="darkorange">
    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" d="M4 8h16M4 16h16" />
  </svg>
);

export default PriorityMediumIcon;
